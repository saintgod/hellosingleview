//
//  ContentView.swift
//  HelloSingleView
//
//  Created by Ludwig Åkermark on 2019-12-03.
//  Copyright © 2019 Ludwig Åkermark. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
